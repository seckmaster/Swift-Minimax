//
//  Board.swift
//  Minimax
//
//  Created by Toni Kocjan on 13/07/2017.
//  Copyright © 2017 Toni. All rights reserved.
//

import Foundation

/// State of the board
enum BoardState: String {
    case X // X won
    case O // O won
    case tie // It's a tie
    case none // Game's still on
    
    var nextPlayer: BoardState {
        if self == .X { return .O }
        if self == .O { return .X }
        return .none
    }
    
    var description: String {
        if self == .X { return "Winner is X" }
        if self == .O { return "Winner is O" }
        if self == .tie { return "It's a tie!" }
        return ""
    }
}

protocol BoardProtocol {
    
    var X: Set<Int> { get }
    var O: Set<Int> { get }
    var size: Int { get }
    var gameState: BoardState { get }
    var availibleMoves: Set<Int> { get }
    
    init?(size: Int)
}

class Board: BoardProtocol {
    
    var X: Set<Int>
    var O: Set<Int>
    var size: Int
    
    required init?(size: Int) {
        guard size > 0 else { return nil }
        
        self.size = size
        self.X = Set()
        self.O = Set()
    }
    
    init?(size: Int, X: Set<Int>, O: Set<Int>) {
        guard size > 0 else { return nil }
        
        self.size = size
        self.X = X
        self.O = O
    }
    
    var gameState: BoardState {
        guard X.count >= 3 else { return .none }
        
        var winner = BoardState.none
        for combination in winningCombinations {
            if combination.isSubset(of: X) {
                // X won
                winner = .X
                break
            }
            if combination.isSubset(of: O) {
                // O won
                winner = .O
                break
            }
        }
        
        if winner != .none {
            return winner
        }
        
        if X.count + O.count == 9 {
            // Tie
            return .tie
        }
        
        return .none
    }


    var availibleMoves: Set<Int> {
        return Set<Int>(0...(size * size - 1)).subtracting(X.union(O))
    }
    
    private lazy var winningCombinations: [Set<Int>] = {
        let rows: () -> [Set<Int>] = {
            var set = [Set<Int>]()
            for j in 0...self.size - 1 {
                var newSet = Set<Int>()
                for i in 0...self.size - 1 {
                    newSet.insert(self.size * j + i)
                }
                set.append(newSet)
            }
            return set
        }
        let cols: () -> [Set<Int>] = {
            var set = [Set<Int>]()
            for j in 0...self.size - 1 {
                var newSet = Set<Int>()
                for i in 0...self.size - 1 {
                    newSet.insert(self.size * i + j)
                }
                set.append(newSet)
            }
            return set
        }
        let diagOne: () -> Set<Int> = {
            var set = Set<Int>()
            for j in 0...self.size - 1 {
                set.insert(self.size * j + j)
            }
            return set
        }
        let diagTwo: () -> Set<Int> = {
            var set = Set<Int>()
            for j in 0...self.size - 1 {
                set.insert(self.size * j + self.size - j - 1)
            }
            return set
        }
        
        return rows() + cols() + [diagOne(), diagTwo()]
    }()
}
