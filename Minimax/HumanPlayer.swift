//
//  HumanPlayer.swift
//  Minimax
//
//  Created by Toni Kocjan on 13/07/2017.
//  Copyright © 2017 Toni. All rights reserved.
//

import Foundation

class HumanPlayer: Player {
    
    func move(board: Board) -> Int {
        var input: Int
        repeat {
            input = Int(readLine() ?? "") ?? -1
        }
        while input == -1 || board.X.contains(input) || board.O.contains(input)
        
        return input
    }
}
