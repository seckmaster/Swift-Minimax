//
//  MinimaxPlayer.swift
//  Minimax
//
//  Created by Toni Kocjan on 13/07/2017.
//  Copyright © 2017 Toni. All rights reserved.
//

import Foundation

class MinimaxPlayer: Player {
    
    func move(board: Board) -> Int {
        let boardSize = board.size * board.size
        
        // Call negamax with default parameters
        return negamax(boardSize, // depth: 3*3=9 for TicTacToe
            board: board, // current board state
            maximizingPlayer: .X, // maximizing player
            alpha: -board.size * board.size - 1, // alpha: -10 for TicTacToe
            beta: board.size * board.size + 1) // beta: +10 for TicTacToe
            .position
    }
    
    // MARK: - Private methods
    
    /// Negamax implementation of minimax algorithm using alpha-beta prunning
    /// Return: tuple containing best position to play and evaluation score for that position
    private func negamax(_ depth: Int, board: Board, maximizingPlayer: BoardState, alpha: Int, beta: Int) -> (evaluation: Int, position: Int) {
        var alpha = alpha
        
        let firstPlayerMoves = board.X
        let secondPlayerMoves = board.O
        
        let opponent = maximizingPlayer == BoardState.X ? BoardState.O : BoardState.X
        
        // evaluate current position
        let state = board.gameState
        if state != .none {
            return (evaluation: [maximizingPlayer: 10 - (9 - depth), opponent: -10 + (9 - depth), BoardState.tie: 0][state]!, position: -1)
        }
        
        // get all availible moves
        var moves = Array(board.availibleMoves)
        // shuffle moves so AI isn't 'boring'
        moves.shuffleInPlace()
        
        // best move
        var bestSquare = -1
        
        var first: Bool = true
        for m in moves {
            // valuation score for this move
            var value = 0
            
            // create new board for each possible move
            let newState = Board(size: board.size,
                                 X: maximizingPlayer == .X ? firstPlayerMoves.union([m]) : firstPlayerMoves,
                                 O: maximizingPlayer == .O ? secondPlayerMoves.union([m]) : secondPlayerMoves)!
            
            if !first {
                value = -negamax(depth - 1, board: newState, maximizingPlayer: opponent, alpha: -alpha - 1, beta: -alpha).evaluation
                if alpha < value && value < beta {
                    value = -negamax(depth - 1, board: newState, maximizingPlayer: opponent, alpha: -beta, beta: -value).evaluation
                }
            }
            else {
                value = -negamax(depth - 1, board: newState, maximizingPlayer: opponent, alpha: -beta, beta: -alpha).evaluation
            }
            first = false
            
            // alpha-beta prunning
            if value >= beta {
                return (value, m)
            }
            // save current best move
            if value > alpha {
                alpha = value
                bestSquare = m
            }
        }
        
        return (alpha, bestSquare)
    }
}
