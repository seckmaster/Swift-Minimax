//
//  Board.swift
//  Minimax
//
//  Created by Toni Kocjan on 12/08/16.
//  Copyright © 2016 Toni. All rights reserved.
//

import Foundation

/// Player protocol
protocol Player {
    func move(board: Board) -> Int
}

protocol Displayable {
    func display()
}

class TicTacToe {
    
    // MARK: - Properties
    
    private var X: Player
    private var O: Player
    private var board: Board
    private var display: Bool
    
    // MARK: - Lifecycle
    
    init(size: Int=3, display: Bool=true, X: Player, O: Player) {
        self.X = X
        self.O = O
        self.board = Board(size: size)!
        self.display = true
    }
    
    // MARK: - Public methods
    
    /// Start game
    func play() {
        var turn = BoardState.X
        
        while true {
            if display {
                print("It's \(turn.rawValue)'s turn:")
            }
            
            if turn == .X {
                board.X.insert(X.move(board: board))
            }
            else {
                board.O.insert(O.move(board: board))
            }
            
            if display {
                board.display()
            }
            
            let currentState = board.gameState
            
            guard currentState == .none else {
                print(currentState.description)
                break
            }
            
            turn = turn.nextPlayer
        }
    }
}

extension Board: Displayable {
    
    /// Print board
    func display() {
        for i in 0...size - 1 {
            for j in 0...size - 1 {
                let position = i * size + j
                if X.contains(position) {
                    print("X ", terminator: "")
                }
                else if O.contains(position) {
                    print("O ", terminator: "")
                }
                else {
                    print("  ", terminator: "")
                }
            }
            print()
        }
        print()
    }
}

/// Extension for Collections
extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex..<endIndex {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}
