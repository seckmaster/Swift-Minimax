//
//  main.swift
//  Minimax
//
//  Created by Toni Kocjan on 12/08/16.
//  Copyright © 2016 Toni. All rights reserved.
//

import Foundation

while true {
    TicTacToe(size: 5, X: MinimaxPlayer(), O: HumanPlayer()).play()
}
